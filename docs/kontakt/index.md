---
hide:
- navigation
- toc
---

# Kontakt

## Adresse

Benedikt Stemmildt<br>
Wesselstraat 1 b<br>
22399 Hamburg

## E-Mail

[benedikt@stemmildt.com](mailto:benedikt@stemmildt.com)

## Telefon

[+49 171 1443 957](tel:+491711443957)

## LinkedIn

[linkedin.com/in/benedikt-stemmildt](https://linkedin.com/in/benedikt-stemmildt)

## XING

[xing.com/profile/Benedikt_Stemmildt](https://www.xing.com/profile/Benedikt_Stemmildt)

## GitLab

[gitlab.com/BeneStem](https://gitlab.com/BeneStem)

## GitHub

[github.com/BeneStem](https://github.com/BeneStem)
