---
hide:
- navigation
---

# Talks

??? info "From Search Results to Insights: Learnings from Statista’s GenerativeAI Journey - 2024"

    ## From Search Results to Insights: Learnings from Statista’s GenerativeAI Journey - 2024

    GenAI services have been adopted successfully in no time across various digital business models, but what if your data has the better answers? How could this innovative technology be combined with a companies knowledge and data?

    In this talk, we delve into the intricacies of Large Language Models (LLMs) and their augmentation with custom data through the use of Retrieval-Augmented Generation (RAG). Learn about Statista’s pioneering journey in moving from extensive search results to concise and well-founded answers, using their LLM-based application, ResearchAI. We will tackle the challenges faced, including building a skilled team for such an emerging technology, the impact of exclusive data sources on answer quality, high product costs and latency per request, and the tendency of LLMs to produce hallucinations despite the availability of accurate data. This session offers a realistic look at the hurdles encountered, and the strategies employed, providing valuable lessons on building and optimizing RAG applications in the real world.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/2d32362f58044c6f9bbe61861d6bac2e" allowfullscreen></iframe>
    </div>

??? info "Why HTMX is crushing React, Vue & Svelte - 2024"

    ## Why HTMX is crushing React, Vue & Svelte - 2024

    Imagine a world where frontend frameworks would have no dependency conflicts, no outdated libraries and no security issues. A world where the gap between frontend and backend is finally closed. Only one deployment is needed and only one service must run.

    A framework that lets you focus on HTML, with no boiler plate in between. No JSON & GraphQL, no serialization, no virtual DOM. Easy to understand and maintain - packed with much higher performance than any existing Javascript framework. And the best part? You have the freedom to choose your backend and preferred typed language.

    Presenting: HTMX, the new approach in writing web applications.

    In this session, we'll show you how simple building an e-commerce shop can be. Using Kotlin, Spring Boot, and plain Tailwind, we'll demonstrate key features that make HTMX a revolutionary approach in writing web applications. Join us to see HTMX in action and explore its potential to transform your web development experience.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/7a85d374415f4dbd94088ffb5075a4d2" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/-o5dn4ts4zM" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "The Big Five for developing Software fast for a long time - 2023"

    ## The Big Five for developing Software fast for a long time - 2023

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/42504a5dceed4ce0bc810f30d56bf38d" allowfullscreen></iframe>
    </div>

??? info "Missverständnisse um Effizienz: Oder wie man ein antifragiler IT-Bereich wird - 2023"

    ## Missverständnisse um Effizienz: Oder wie man ein antifragiler IT-Bereich wird - 2023

    Effizient zu sein, ist der am meisten fehlgebrauchte und missverstandene Begriff in IT-Bereichen.
    Über Effizienz zu sprechen, löst sofort bestimmte Reaktionen bei Management und Mitarbeitern aus.
    Meistens denkt man dabei an Ressourceneffizienz und Automatisierung:
    Kosten reduzieren, Overhead verringern, usw. Habe ich recht?
    Aber über Effizienz in diesem engen Kontext nachzudenken, reicht heutzutage nicht mehr aus.
    In einer immer komplexeren und sich schneller verändernden Welt, die von mehreren Krisen und unvorhergesehenen Ereignissen erschüttert wird, müssen Unternehmen und deren IT-Bereiche zu antifragilen Versionen von sich selbst werden.
    Dieser Vortrag wird aufzeigen, was Effizienz aus einer ganzheitlichen Sicht bedeutet und gibt neue Perspektiven auf gängige Missverständnisse.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/b45c8b08d342444488bd889bbf528eb4" allowfullscreen></iframe>
    </div>

??? info "Verticalization: Team Topologies building SCS along the Business Model Canvas - 2023"

    ## Verticalization: Team Topologies building SCS along the Business Model Canvas - 2023

    The TalentFormation Network rebuilds companies based on the three horizon model. The concept of Verticalization is the work horse for the first horizon to get the company on a solid base for improvement and extension of the business model. The course of verticalization is building products and technology organizations reflecting the ideas of Team Topologies. Those organizations focus on development of Self-Contained Systems (SCS) which are aligned with the Business Model Canvas.
    All models are wrong but some are useful: In this talk, Christoph and Benedikt will explain the ideas we combined to make them even more useful.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/d5f128ea33c042c3b454786b28a136fa" allowfullscreen></iframe>
    </div>

??? info "Lose Kopplung im Frontend mit 'Hotwire: HTML over the wire' oder auch nieder mit den SPAs - 2023"

    ## Lose Kopplung im Frontend mit 'Hotwire: HTML over the wire' oder auch nieder mit den SPAs - 2023

    Heutzutage führt kein Weg an Single-Page Applications vorbei. Ob React, Angular, VueJS oder eines der anderen Frameworks. Die Standardantwort auf die Frage nach der Frontend-Architektur heißt SPA. Doch was kaum jemand bemerkt:

    Die SPA-Idee ist legacy! Mit AngularJS wurde vor 10 Jahren diese Idee bereits umgesetzt.

    Ich möchte im Vortrag zeigen, aufgrund welcher Frontend-Probleme man ursprünglich SPAs entwickelt hat und ob es für diese Probleme nicht heutzutage innovativere Lösungen gibt: Hier kommt Hotwire ins Spiel!

    Zielpublikum: Architekt:innen, Entwickler:innen, Frontend, Backend
    Voraussetzungen: Keine
    Schwierigkeitsgrad: Fortgeschritten

    Extended Abstract:
    Hier kommt Hotwire ins Spiel, das mit einer sehr schlauen, auf low-level Standards setzenden Lösung ganz neue Möglichkeiten für saubere Architekturen schafft, ohne die UX-Vorteile einer SPA zu verlieren!
    Ganz anders als SPAs lässt sich mit Hotwire eine lose Kopplung und geringe Abhängigkeiten zwischen verschiedenen Teams wahren und so Skalierungsfähigkeit erhalten.

    Wenn ihr die Technologie kennenlernen und gleichzeitig ihren positiven Effekt auf Teams und Abhängigkeiten erfahren wollt, dann kommt gern vorbei!

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/13a7392e10b3486799a65c4cf5787182" allowfullscreen></iframe>
    </div>

??? info "DDD-Projekte profitieren von funktionaler Architektur - 2023"

    ## DDD-Projekte profitieren von funktionaler Architektur - 2023

    Funktionale Programmier:innen empfehlen meist "FP-first". Dieser puristische Ansatz passt leider nicht zu den Realitäten existierender Projekte, die objektorientiert auf DDD aufsetzen. Schade, weil diese Projekte von funktionalen Techniken profitieren können.
    Der Vortrag beschreibt eine Kooperation zwischen Blume2000 (fest in OO-Hand) und der Active Group (alles FP-Puristen), um Probleme in einer hexagonalen DDD-Architektur (implementiert in Kotlin) mit funktionalen Techniken zu lösen.

    Zielpublikum: Entwickler:innen, Architekt:innen
    Voraussetzungen: OO-Grundkenntnisse
    Schwierigkeitsgrad: Fortgeschritten

    Extended Abstract:
    Dazu gehören:
    - Validierung mit applikativen Funktoren
    - freie Monaden, um die Domäne von den Ports zu trennen
    - funktionale Dependency Injection, auch mit Monaden

    Wir zeigen, wie wir's gemacht haben - und wie auch andere Projekte von funktionaler Programmierung profitieren können.
    Das alles harmoniert wunderbar mit dem Kotlin/Spring-Boot-Kontext des Projekts.

    Es gab aber durchaus auch Reibungspunkte: Die strikte Domänen-/Technik-Separierung des hexagonalen Modells hilft Entwickler:innen, sich zurechtzufinden - funktionale Programmierer:innen benutzen normalerweise feingranularere Abstraktionen im Rahmen der Functional-Core/Imperative-Shell-Architektur. Entsprechend präsentieren haben wir auch einige Vorschläge, wie Spring Boot verbessert werden könnte, um diesen Ansatz besser zu unterstützen.

??? info "What it takes to be fast: A tale about responsibility, accountability and independence - 2022"

    ## What it takes to be fast: A tale about responsibility, accountability and independence - 2022

    Nowadays being fast is one of the most important goals of every company. Being fast at entering a new market, being fast at developing new features, being fast at innovating new products. Time To Market is the fuel for every company’s Build, Measure, Learn cycle. If you run through it faster you learn much more in a shorter time, giving you a huge advantage over competitors. This is pretty obvious, but how do you make this happen? Benedikt would like to tell you a story about the power of independence.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/b2a60c0792fe4e7da772f95360dab6d9" allowfullscreen></iframe>
    </div>

??? info "Lose Kopplung im Frontend mit 'Hotwire: HTML over the wire' oder auch Tod den Single Page Applications – 2022"

    ## Lose Kopplung im Frontend mit 'Hotwire: HTML over the wire' oder auch Tod den Single Page Applications – 2022

    Heutzutage führt kein Weg an Single Page Applications vorbei. Ob React, Angular, VueJS oder eines der anderen Frameworks. Die Standardantwort auf die Frage nach der Frontend-Architektur heißt SPA. Doch was kaum jemand bemerkt: Die SPA Idee ist legacy! Mit AngularJS wurde vor 10 Jahren diese Idee breit umgesetzt.  Ich möchte im Vortrag zeigen, aufgrund welcher Frontend-Probleme man ursprünglich SPAs entwickelt hat und ob es für diese Probleme nicht heutzutage innovativere Lösungen gibt:Hier kommt Hotwire ins Spiel, dass mit einer sehr schlauen, auf low-level Standards setzenden, Lösung ganz neue Möglichkeiten für saubere Architekturen schafft ohne die UX-Vorteile einer SPA zu verlieren!  Ganz anders als SPAs lässt sich mit Hotwire eine lose Kopplung und geringe Abhängigkeiten zwischen verschiedenen Teams wahren und so Skalierungsfähigkeit erhalten.  Mehr dazu im Talk, kommt vorbei!

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/13a7392e10b3486799a65c4cf5787182" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/GHaWrC5QTPc" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Funktionale Architektur mit Kotlin – 2022"

    ## Funktionale Architektur mit Kotlin – 2022

    Kotlin wirbt zwar mit der Unterstützung funktionaler Programmierkonzepte, in der Praxis muss man sich aber schon anstrengen, um sie in der Praxis auch einzusetzen. (Immerhin ist es in Kotlin leichter als in Java.) Dabei sind einige von ihnen durchaus nützlich bei der Entwicklung einer sauberen Architektur: Bei der zuverlässigen Organisation und Validierung von Daten und der flexiblen Dependency Injection mit Monaden zum Beispiel. Diese Techniken sind eine ideale Ergänzung für ein DDD-Projekt und können auch nachträglich noch eingebaut werden. Der Vortrag zeigt, wo funktionale Programmierung am meisten bringt in Kotlin, und wie sich damit die Architektur verbessern lässt.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/34bba3238ec64f9283a9b7461bda1dcd" allowfullscreen></iframe>
    </div>

??? info "Different – 2022"

    ## Different – 2022

    Wie schafft man ideale Arbeitsbedingungen in Tech-Organisationen, die der Organisation zudem einen immensen Wettbewerbsvorteil verschaffen?

    <iframe src="https://different-by-stefan-luther.podigee.io/1015-benedikt-stemmildt/embed?context=external" style="border: 0" border="0" width="100%"></iframe>

??? info "Agil durch „Accelerate“ – 2022"

    ## Agil durch „Accelerate“ – 2022

    „Accelerate“ ist ein Buch, das CIOs eine Art Baukasten an die Hand gibt, wie sie agile Methoden so einführen können, dass sie messbaren Business-Erfolg liefern. Benedikt Stemmildt, CIO von BLUME2000, hat genau das umgesetzt und spricht mit uns über seine Erfahrungen aus dem Change-Prozess, beleuchtet Dos und Don’ts und gibt praktische Tipps für die Einführung an die Hand.

    <iframe src="https://idgtechtalk.podigee.io/43-37-agil-durch-accelerate-mit-benedikt-stemmildt/embed?context=external" style="border: 0" border="0" width="100%"></iframe>

??? info "Responsible Architecture – 2021"

    ## Responsible Architecture – 2021

    Geschwindigkeit, Effektivität und Freude durch unternehmerische Verantwortung

    [Read it here](https://www.informatik-aktuell.de/entwicklung/methoden/responsible-architecture.html)

??? info "Single-Page-Applications sind legacy, wir brauchen eine Innovation! – 2021"

    ## Single-Page-Applications sind legacy, wir brauchen eine Innovation! – 2021

    Heutzutage führt kein Weg an Single Page Applications vorbei. Ob React, Angular, VueJS oder eines der anderen Frameworks. Die Standardantwort auf die Frage nach der Frontend-Architektur heißt SPA.
    Doch was kaum jemand bemerkt: die SPA Idee ist legacy! Mit AngularJS wurde vor 10 Jahren diese Idee umgesetzt.
    Ich möchte im Vortrag zeigen, aufgrund welcher Frontend-Probleme man ursprünglich SPAs entwickelt hat und ob es für diese Probleme nicht heutzutage innovativere Lösungen gibt.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/2511215eae074a3f8b5195cb17d13251" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/9KEYt56BMXM" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Wie Benedikt Stemmildt, CIO bei Blume2000, die Webarchitektur jenseits von Microservices organisiert – 2021"

    ## Wie Benedikt Stemmildt, CIO bei Blume2000, die Webarchitektur jenseits von Microservices organisiert – 2021

    Microservices sind ein großer Trend. Aber sie verhindern Geschwindigkeit, ist Benedikt Stemmildt, CIO bei Blume2000, überzeugt. Deshalb wird dort etwas völlig Entgegengesetztes gemacht: Man setzt auf die komplette Trennung der Fachlichkeit, Organisation – und schließlich auch der Technik. Wie das funktionieren kann? Das beschreibt er in seinem Vortrag „Geschwindigkeit ohne Microservices: Wie man mit guten alten HTML-Seiten und Makrodiensten vorankommt“, den er auf der Digitale Leute Summit 2021 gehalten hat.

    [Read it here](https://www.digitale-leute.de/interview/wie-benedikt-stemmildt-cio-bei-blume2000-die-webarchitektur-jenseits-von-microservices-organisiert/)

??? info "IT-Geschwindigkeit geht vor Effizienz – 2021"

    ## IT-Geschwindigkeit geht vor Effizienz – 2021

    Die IT des Onlinehändlers Blume 2000 soll neue Features künftig schneller ausliefern. Dazu organisierte CIO Benedikt Stemmildt die traditionellen technischen Abteilungen in fachbezogene Teams um und spaltete die IT-Landschaft seines Bereichs auf.

    [Read it here](https://www.cio.de/a/it-geschwindigkeit-geht-vor-effizienz)

??? info "Documentation is Dead, Long Live Documentation! – 2021"

    ## Documentation is Dead, Long Live Documentation! – 2021

    Documentation is important—in companies of all sizes—but developer want to write code, and we don't want to log into a second, bloated system to document our work. Why couldn't docs be more like writing code—and more fun?

    It is possible! These are several markdown or ascii-based frameworks that are accessible to developers and non-technical users alike, and they can greatly simplify the documentation process. We'll tell the story of our docs journey, integrating a framework that and hosting with GitLab Pages. We'll also highlight tips and tricks to help you get started!

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/e5b30614429e4c26a558d6d898320916" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/UmzgPF4QhJI" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Enge Zusammenarbeit trotz Homeoffice: Wie BLUME2000 mit Mob-Programming seinen Web-Shop erneuert – 2021"

    ## Enge Zusammenarbeit trotz Homeoffice: Wie BLUME2000 mit Mob-Programming seinen Web-Shop erneuert – 2021

    Mitten in der Pandemie beginnt BLUME2000 mit dem Neubau seiner erfolgreichen E-Commerce Plattform. Mit Remote-Mob-Programming und neuen Ansetzen zum Wissenstransfer liefern mehrere Teams den MVP in wenigen Monaten. Erfahre mehr über das Projekt Setup, wie iteratec die Teams unterstützt, die eingesetzten Technologien und die Lessons learned einem der spannendsten Hamburger E-Com Projekte 2021.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/dbb44667ab3f4c3cad330d25ae5cf539" allowfullscreen></iframe>
    </div>

??? info "Speed without the microservice hype: How BLUME2000 performs with good old HTML websites and expertly cut macro services – 2021"

    ## Speed without the microservice hype: How BLUME2000 performs with good old HTML websites and expertly cut macro services – 2021

    Many companies have thrown themselves too carelessly into these supposedly all-solving approaches like the microservice and single page application hype and are now being slowed down by even more complex infrastructure. In his talk Benedikt shows us how to become incredibly fast with good old HTML techniques and expertly cut macro services paired with DevOps practices. He explains how to become truly successful when leadership, expertise, organization, and technology interact mutually.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/d4fd3e275dbf4f68878f79457b65f757" allowfullscreen></iframe>
    </div>

??? info "Durch die Blume, mit Rosenstrauß zur Achterbahn – 2020"

    ## Durch die Blume, mit Rosenstrauß zur Achterbahn – 2020

    Omnichannel, Digital Business, Vertikalisierung, agile Programmierung, Apps. Als kleines oder mittelständisches
    Unternehmen kann einen das schon mal überfordern. Wie man trotzdem mit den großen mithalten und sie sogar überholen
    kann, wird euch Benedikt Stemmildt, Lead Software Architekt, in dieser Session näher bringen.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/U73UaWXCTZ4" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Warum Quarkus.io geiler ist als Spring – 2019"

    ## Warum Quarkus.io geiler ist als Spring – 2019

    Mikroservices ohne Spring Boot sind nicht wegzudenken. Aber ist Spring wirklich so großartig wie alle immer sagen?
    Spring kommt aus einer Zeit als man Monolithen gebaut hat, die dann das OPS-Team on-premise auf riesigen Servern
    gehostet hat. Viele Kern-Features und Interna sind geprägt von dieser Zeit. Neue Operations-Modelle wie Serverless und
    DevOps zwingen ein Umdenken. Quarkus.io macht vor, wie schlank und schnell ein Web-Framework sein kann. Entstanden in
    unserer Zeit bietet es genau das, was man meint, wenn man von Mikroservices spricht. In diesem talk will ich gemeinsam
    mit euch mehrere Quarkus Services aufsetzten und diese in Kunernetes deployen. Das Zusammenspiel und die Orchestrierung
    werden durch Istio und Kiali deutlich gemacht.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/14c1790adb424b399a8397a31e4b15b7" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/yA9TocpMthI" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "ETL 2.0: Produktdatenverarbeitung von Batch zu Event getrieben – 2019"

    ## ETL 2.0: Produktdatenverarbeitung von Batch zu Event getrieben – 2019

    Seit vielen Jahren verarbeitet Breuninger seine Produktdaten mit klassischen Batch Jobs. Durch die immer höher werdenden
    fachlichen Anforderungen ist diese Art der Verarbeitung nicht mehr möglich. Wir haben daher unsere
    Verarbeitungs-Pipeline auf einen event-getriebenen Ansatz umgebaut. Diese neue Pipeline, ihre Vor- und Nachteile wollen
    wir euch in diesem Talk vorstellen. Verwendete Technologien sind Java11, Kafka, MongoDB und Atom-Feeds über HTTP & JSON.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/e6835670956a413889eb1ea27b9a2656" allowfullscreen></iframe>
    </div>

??? info "Persistence with the head in the cloud – 2018"

    ## Persistence with the head in the cloud – 2018

    Microservices sind in aller Munde, auch das Konzept der Self-Contained Systems hat in letzter Zeit an Bekanntheit
    gewonnen. Doch wie unterscheiden sich die beiden Konzepte? Lassen sich Microservices mit Self-Contained Systems sinnvoll
    kombinieren oder gibt es Widersprüche und Stolperfallen zu beachten? Dieser Talk wird unter anderem Antworten auf diese
    Fragen geben und außerdem Einblicke auf den Einsatz der Konzepte in der Multi-Channel Plattform von Breuninger geben.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/0c17d1cb80044839bc98bfbab6036b52" allowfullscreen></iframe>
    </div>

??? info "Alles Klamotte oder was? Erfahrungsbericht von der Herausforderung einem Bären Beine zu machen! – 2018"

    ## Alles Klamotte oder was? Erfahrungsbericht von der Herausforderung einem Bären Beine zu machen! – 2018

    Vertikalisierung, Omnichannel, Digital Business, agile Programmierung, Apps – und mittendrin ein 136 Jahre altes
    Einzelhandelsunternehmen, das man nicht erwartet hätte. Das Traditionsunternehmen Breuninger ist grundsätzlich nicht
    besonders bekannt dafür, ein großer Player im IT-Business zu sein, verbindet man das Unternehmen doch eher mit
    exquisiter Bekleidung und vielleicht noch mit der Rolle als Herrenausstatter des VFB. Wie das alles trotzdem perfekt
    zusammenpasst, werden euch Katja Burkert, Head of Software Engineering und Benedikt Stemmildt, Lead Software Architekt,
    in einer Keynote näherbringen. Dabei sprechen Katja und Bene über die Herausforderungen und Chancen einer vertikalen
    Architektur, „sexy“ Code und warum Vertikalisierung in der Softwarearchitektur bitter nötig ist. #cloud

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/faf3e771c2954a0a9b499cd2533cc8e4" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/76S1vIbt1-Y" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Self-Contained-Systems: Mehr als nur eine geordnete Menge von Microservices? – 2018"

    ## Self-Contained-Systems: Mehr als nur eine geordnete Menge von Microservices? – 2018

    Microservices sind in aller Munde, auch das Konzept der Self-Contained Systems hat in letzter Zeit an Bekanntheit
    gewonnen. Doch wie unterscheiden sich die beiden Konzepte? Lassen sich Microservices mit Self-Contained Systems sinnvoll
    kombinieren oder gibt es Widersprüche und Stolperfallen zu beachten? Dieser Talk wird unter anderem Antworten auf diese
    Fragen geben und außerdem Einblicke auf den Einsatz der Konzepte in der Multi-Channel Plattform von Breuninger geben.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/ae5bbc3578a34cc9810c77ae7701aa76" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/nHeHVvAKsWs" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Going Reactive: Eine Einführung in die reaktive Programmierung – 2017"

    ## Going Reactive: Eine Einführung in die reaktive Programmierung – 2017

    Die aktuelle Marktentwicklung treibt Onlineshops dazu sich vom einem monolithischen System hin zu einem verbundenen
    System aus vielen Microservices zu entwickeln. Diese kommunizieren in der Regel mit vielen Schnittstellen untereinander
    Sowohl Clients außerhalb des Shop-Systems, wie Apps, IoT Devices, Smartphones als auch die Clients innerhalb des
    Shop-Systems sind auf realtime Daten angewiesen. Bisher genutzte iterative Programmierparadigmen kommen hier an ihre
    Grenzen. Im Vortrag wird auf diese Probleme detailliert eingegangen und das Paradigma des Reactive Programmings genau
    und strukturiert erörtert. Abschließend wird der Code einer Applikation gezeigt, die das Rx Paradigma nutzt, um viele
    Daten von Datenbank bis Website-Frontend zu streamen.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/71e46bf9b6dd499badfc5369c3123935" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/YnladsQPqYk" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Offene Ökosysteme und verteilte Architekturen – 2017"

    ## Offene Ökosysteme und verteilte Architekturen – 2017

    Die aktuelle Marktentwicklung treibt E-Commerce Systeme dazu sich von einem abgeschlossenem Shop hin zu einem offenen
    Ökosystem zu entwickeln. Dafür ist zwingend notwendig Funktionalitäten über eine externe API zu veröffentlichen.
    Verschiedenste Clients wie zum Beispiel native Apps, Social-Media-Sites, IoT-Devices, Stationärhandel und externe
    Websites sollen sich flexibel anbinden können. Im Vortrag wird die verteilte Microservice-Architektur von Breuninger
    vorgestellt. Dabei wird vor allem auf Motivation & praktische Learnings eingegangen. Daraufhin werden
    Architektur-Lösungsansätze für öffentliche und interne APIs innerhalb von verteilten Architekturen vorgestellt und
    bewertet. Im Fazit wird darauf eingegangen, welche dieser Architekturen Breuninger für sich ausgewählt hat und von
    praktischen Erfahrungen berichtet.

??? info "Zack die Ente und der API-Client spricht – 2017"

    ## Zack die Ente und der API-Client spricht – 2017

    Die aktuelle Marktentwicklung treibt otto.de dazu sich vom einem abgeschlossenem Shop hin zu einem offenen Ökosystem zu
    entwickeln. Dafür ist zwingend notwendig Shop-Funktionalitäten über eine API zu veröffentlichen. Verschiedenste Clients
    wie zum Beispiel native Apps, Social-Media-Sites, IoT-Devices und externe Websites sollen sich flexibel anbinden können.
    Im Vortrag wird die Microservice-Architektur von otto.de und der Aufbau unserer externen API (
    Java/Spring-Security/OAuth2) vorgestellt. Es wird auf Herausforderungen und Lösungen in verteilten Architekturen
    eingegangen. Abschließend zeigen wir anhand einer Live-Demo wie einfach sich externe Clients und überraschende
    Anwendungsfälle über die API umsetzen lassen.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/6a31d019d6d5435abc8a872f3df05487" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/kGqODkh1TJc" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>

??? info "Microservices – Hype oder schon Realität? – 2016"

    ## Microservices – Hype oder schon Realität? – 2016

    Im E-Commerce zählt die Devise: agiler, schneller und innovativer. Um heute erfolgreich zu sein, müssen bestehende
    Geschäftsmodelle regelmäßig hinterfragt und kontinuierlich weiterentwickelt werden. Otto.de ist eine der größten
    E-Commerce Plattformen Deutschlands, und wir stellen uns diesen Herausforderungen. Die Entwicklung von otto.de folgt
    einer strikten, vertikal zugeschnittenen shared nothing Architekutur. Um uns nicht wieder mit den vielfältigen Probleme
    monolithischer Anwendungen beschäftigen zu müssen, entwickeln wir otto.de zusätzlich in Microservices und haben unsere
    Infrastruktur dementsprechend verändert. Im Vortrag stehen neben der Architektur von otto.de der Umgang Microservices im
    Mittelpunkt. Dabei steht nicht nur die reine Entwicklung im Fokus der Betrachtung, sondern auch Real-Life-Aspekte wie
    Deployment und Betrieb.

    <div class="iframe-container">
      <iframe class="responsive-iframe" src="//speakerdeck.com/player/8da85d1d344c4adea5d86302c152e717" allowfullscreen></iframe>
    </div>
    <br>
    <div class="iframe-container">
      <iframe class="responsive-iframe" src="https://www.youtube-nocookie.com/embed/zoI5pYW1AB0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
    </div>
