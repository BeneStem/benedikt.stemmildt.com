---
hide:
- navigation
---

# Education

## Books I ❤️

### Pile Of Shame

* A Type of Programming - Renzo Carbonara
* Software Architecture for Developers - Simon Brown
* The Five Dysfunctions of a Team: A Leadership Fable - Patrick Lencioni
* Dynamic Reteaming: The Art and Wisdom of Changing Teams - Heidi Helfand
* Shape Up: Stop Running in Circles and Ship Work that Matters – Ryan Singer
* Sooner, Safer, Happier - Jonathan Smart
* Measure What Matters: OKRs: The Simple Idea that Drives 10x Growth - John Doerr
* Quantenträume: Erzählungen aus China über Künstliche Intelligenz - Jing Dr. Bartz, Hao Jingfang, Qiufan Chen, Wang Jinkang
* Der Fall des Präsidenten - Marc Elsberg
* GIER: Wie weit würdest du gehen? - Marc Elsberg

### Organization

* Accelerate: The Science of Lean Software and Devops: Building and Scaling High Performing Technology Organizations –
  Nicole Forsgren Phd, Jez Humble, Gene Kim
* Scaling Up: How a Few Companies Make It...and Why the Rest Don't, Rockefeller Habits 2.0  - Verne Harnish
* Team Topologies: Organizing Business and Technology Teams for Fast Flow - Matthew Skelton, Manuel Pais
* Lean Startup: Schnell, risikolos und erfolgreich Unternehmen gründen – Eric Ries
* Art of Scalability, The: Scalable Web Architecture, Processes, and Organizations for the Modern Enterprise – Martin L.
  Abbott

### Leadership

* Leadership Is Language: The Hidden Power of What You Say and What You Don't - L. David Marquet
* Turn the Ship Around!: A True Story of Turning Followers into Leaders - L. David Marquet
* The Phoenix Project - Gene Kim, Kevin Behr, and George Spafford
* The Unicorn Project: A Novel about Developers, Digital Disruption, and Thriving in the Age of Data - Gene Kim

### Programming

* Micro Frontends in Action - Michael Geers
* Domain Modeling Made Functional: Tackle Software Complexity with Domain-Driven Design and F# – Scott Wlaschin
* Implementing Domain-Driven Design – Vaughn Vernon
* Patterns of Enterprise Application Architecture – Martin Fowler
* The Little Elixir & OTP Guidebook – Benjamin Tan Wei Hao

### Fun

* The Better Angels of Our Nature: Why Violence Has Declined – Steven Pinker
* Die Trisolaris-Trilogie - Cixin Liu
* ZERO: Sie wissen, was du tust - Marc Elsberg
* HELIX: Sie werden uns ersetzen - Marc Elsberg
* BLACKOUT: Morgen ist es zu spät - Marc Elsberg
* Der Schwarm - Frank Schätzing
* Luna Series - Ian McDonald
* 1984 – George Orwell
* Bad Blood: Secrets and Lies in Silicon Valley – John Carreyrou

## Zertifikate

* Java Web Hacking & Hardening
* Whitehat-Hacker Security Certification I
* Whitehat-Hacker Security Certification II
* Konfliktmanagement
* Potenzial der eigenen Stärke

## BSc Wirtschaftsinformatik – FH Nordakademie

### 💾 Informatik

* Theoretische Grundlagen
* Technische Grundlagen
* Programmierung
* Algorithmen & Datenstrukturen
* Programmiermethodik
* Softwareproduktion
* Datenbanksysteme
* Mathematik

### 🌐 Wirtschaftsinformatik

* Internetgrundlagen
* Internetanwendungsarchitekturen
* Betriebswirtschaftliche Anwendungen
* Projektmanagement
* Geschäftsprozessmodellierung & Qualitätsmanagement

### 💵 Wirtschaft

* Rechnungswesen
* VWL
* BWL
* Marketing
* Logistik
* Controlling
