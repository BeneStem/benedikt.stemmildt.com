---
hide:
- navigation
---

# Experience

## Personal Projects

### [Gitlab](http://gitlab.com/benestem)

* [Verticalization Service One](https://gitlab.com/BeneStem/verticalization-example-service-one)
* [Verticalization Service Two](https://gitlab.com/BeneStem/verticalization-example-service-two)
* [Verticalization Documentation](https://gitlab.com/BeneStem/verticalization-documentation)
* [Verticalization Design System](https://gitlab.com/BeneStem/verticalization-design-system)
* [Verticalization Frontend Proxy](https://gitlab.com/BeneStem/verticalization-frontend-proxy)

### [Github](http://github.com/benestem)

* [Spring-Boot-Starter-Breuninger](https://github.com/e-breuninger/spring-boot-starter-breuninger)
* [Edison-Microservice](https://github.com/otto-de/edison-microservice)
* [Turing-Microservice](https://github.com/otto-de-legacy/turing-microservice)

## Employment History

### CTO bei [TalentFormation](http://talentformation.com/): 08/2022 – heute

### CIO bei der [BLUME2000 SE](http://blume2000.de/): 04/2020 – 08/2022

In der Rolle des CIO habe ich bei BLUME2000 die Verantwortung für alle Mitarbeiter der E-commerce IT Abteilung übernommen.
Gemeinsam mit meinem Team haben wir die kundenorientierten Systeme betreut.
Da die Abteilung für die auf uns zukommenden strategischen Aufgaben nicht gut aufgestellt war habe ich die Ebenen Produkt, Organisation und Technik vollständig erneuert.
Um dauerhaft in eine kontinuierliche Produktentwicklung überzugehen haben wir ein letztes Projekt durchgeführt. 
In diesem Zug ist der auch ein komplett neuer Online-Shop mit neuem technischen Unterbau entstanden.
Weitere Details dazu sind in dieser Präsentation zu finden: https://speakerdeck.com/benestem/what-it-takes-to-be-fast

### Lead Software Architect bei der [Breuninger GmbH & Co.](http://breuninger.com/): 12/2017 – 04/2020

Als Lead Software Architect war ich für die gesamte Online-Shop Plattform, die von 13 Teams (40 interne & 30 externe
Entwickler) weiterentwickelt wird, verantwortlich. Während meiner Tätigkeit haben wir den kompletten Shop, in DE und AT,
neu gelaunched, in die Schweiz expandiert und unser Geschäftsmodell um das Marktplatz Konzept erweitert. Und das, ohne
nennenswerte technische Schulden aufzubauen, die uns in der Weiterentwicklung verlangsamen würden. Außerdem haben wir
eine Daten Plattform aufgebaut, die es uns ermöglicht erste Data Science Funktionen auszuprobieren. Durch mein Mitwirken
konnten wir 2018 im Online-Bestellumsatz um 35% wachsen. Dabei …

* … fokussierte ich die Teams auf die entscheidenden technologischen Aufgaben und Herausforderungen.
* … plante, gestaltete und hielt ich die architektonischen Lösungen und Weiterentwicklungen der Produkte nach.
* … organisierte und gab ich interne, sowie externe Vorträge, Konferenzen und Workshops, um unsere Mitarbeiter
  weiterzubilden und die Arbeitgebermarke zu stärken.
* … führte ich mehrere Bewerbungs- und Entwicklungsgespräche die Woche, um unsere Teams mit den richtigen Menschen zu
  verstärken.

Besonders bestärkt in meiner Arbeit war ich dadurch, dass die von mir etablierte, vertikale Architektur unsere Teams
kundenzentriert, datengetrieben, unabhängig, selbstorganisiert und kosteneffizient gemacht hat.

### Software Engineer bei der [Otto GmbH & Co. KG](http://otto.de/): 04/2013 – 11/2017

Nach der Übernahme aus dem Dualen Studium war ich zunächst als Software Engineer Teil eines SCRUM Teams von 10 Personen.
Wir waren verantwortlich für das Design und die Implementierung eines neuen Backoffice-Systems zur Bewirtschaftung des
kompletten Online-Shops. Meine Aufgabe bestand darin, mit dem Product Owner neue Funktionen zu entwickeln und diese mit
meinem Team zu implementieren. Sehr stolz bin ich darauf, dass ich mit wachsender Erfahrung und Überzeugungsarbeit,
gemeinsam mit einem Kollegen, die Geschäftsführung überzeugen konnte ein neues Team, mit dem Fokus auf Social-Media
Funktionen, zu gründen, dessen technische Co-Führung ich übernahm. Im Gründungsjahr konnte das Team nach zweiwöchiger
Entwicklungszeit durch eine kleine Funktion, gemeinsam mit Guido Maria Kretschmer, einen dreistelligen Millionenumsatz
erzeugen. Des Weiteren habe ich die Studenten- & Azubi-Betreuung und das Personalmarketing unterstützt, um gezielter
Bewerber für uns auszubilden und zu gewinnen.

### Dualer Student bei der [Otto GmbH & Co. KG](http://otto.de/): 10/2009 – 03/2013

In der Rolle des Dualen Studenten habe ich diverse Bereiche durchlaufen, um die komplette Bandbreite des Unternehmens
kennenzulernen. Darunter waren das Otto Controlling, die SAP HR ABAP Entwicklung, das baumarkt direkt Controlling, die
eigenständige Azubifirma, der MMDB E-Commerce Bereich, Corporate Strategie und der Startup Inkubator Liquid Labs, an
dessen Gründung ich mitgewirkt habe. Dort hatte ich auch den längsten Einsatzzeitraum, indem wir mehrere Startups
initiiert haben. Als erfolgreichstes daraus hat sich Risk.Ident ausgegründet.

## Teaching History

### Inspirer & Gründungsmitglied der [Hacker-School UG](http://hacker-school.de/): 03/2014 – heute

Die Hacker School UG begeistert Kinder und Jugendliche zwischen 11 und 18 Jahren für Digitalisierung und Programmieren.
Zu meinen Aufgaben als Inspirer gehört es Kinder in regelmäßigen Wochenendkursen von Technologie zu begeistern. Seit
Anfang 2018 bin ich außerdem als Organisator in Stuttgart eingesetzt, wo ich die Veranstaltungen im Mercedes-Benz-Museum
und bei Breuninger plane und durchführe. Großartig ist, dass wir bei unseren bisher drei Sessions im Breuninger, mit 20
freiwilligen Helfern und Unterstützung der Unternehmensleitung, jeweils 80 Kinder inspirieren konnten.

### Dozent an der [DHBW Mosbach](http://mosbach.dhbw.de/): 12/2018 – 04/2020

Als Dozent halte ich für zwei Jahrgänge mit jeweils 40 Studenten die Vorlesungen „Cloud-Computing“ und „Web-Apps“. Mir
ist besonders wichtig, den Studenten praktisches und theoretisches Wissen zu vermitteln und deutlich zu machen, dass
auch der Dozent mit jeder Vorlesung Neues lernt. So kann ich vom aktuellen Wissen und den verschiedenen Hintergründen
der Studenten profitieren und bleibe dadurch immer über neue Technologien informiert. Mich ehrt, dass die Studenten gern
zu meinen Vorlesungen gehen und mich häufig, freiwillig als Prüfer für ihre Projektarbeiten auswählen.

### Dozent an der [FH Nordakademie](http://nordakademie.de/): 08/2013 – 03/2018

Zunächst habe ich Wochenendseminare veranstaltet und später die Vorlesung „Internet-Anwendungsarchitekturen“ übernommen.
Dies ist die herausforderndste Vorlesung im Wirtschaftsinformatik Programm der FH und wird mit einer umfassenden
Hausarbeit abgeschlossen. Ich habe mich sehr gefreut, als die FH mich direkt nach meinem dortigen Studium als Dozent für
diesen Kurs übernommen hat, obwohl es diverse weitere Bewerber gab.
