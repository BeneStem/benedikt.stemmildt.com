---
hide:
- navigation
---

# Skills

??? example "Languages"

    ## Languages

    * Kotlin
    * Typescript
    * Java
    * Javascript
    * HTML
    * CSS, Sass, PostCSS
    * Rust
    * Scala
    * Clojure
    * Terraform
    * Elixir
    * Ruby
    * Go
    * Haskell
    * SQL

??? example "Frameworks"

    ## Frameworks

    * Spring Boot
    * Quarkus
    * Arrow.kt
    * Actix
    * Phoenix
    * Rails
    * Wicket
    * Hotwire
    * Vue.js
    * Pinia
    * React
    * Tailwind
    * Vite
    * Rollup
    * Webpack
    * Es4x
    * ActiveJ
    * Fiber
    * Kover
    * Detekt
    * Klint
    * JUnit
    * Servant
    * Commitlint
    * Reactor
    * Solid.js
    * Jest
    * Stylelint
    * Eslint
    * Vert.x
    * Prettier

??? example "Platforms"

    ## Platforms

    * Amazon Web Services (AWS)
    * Kubernetes
    * Docker
    * Google Cloud Platform (GCP)
    * Podman

??? example "Tools"

    ## Tools

    * IntelliJ IDEA
    * GitLab
    * Datadog
    * Kibana (ELK)
    * SpeedCurve
    * Jenkins
    * Jira / Confluence
    * Opsgenie
    * Space
    * Miro
    * Figma

??? example "Databases & Queues"

    ## Databases, Queues & Dataformats

    * Protobuf / Avro
    * MongoDB
    * PostgreSQL
    * Apache Kafka
    * Elasticsearch

??? example "Methodologies"

    ## Methodologies

    * Self Contained Systems
    * Domain Driven Design
    * Reactive Programming
    * Functional Programming
    * REST
    * CI / CD
    * DevOps
    * SCRUM / Kanban
    * XP
