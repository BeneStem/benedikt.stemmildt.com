---
hide:
- navigation
- toc
---

# Benedikt Stemmildt

![About](./assets/images/about.jpeg){: align=right .about-image }

Leidenschaftlicher Software Architekt, Full-Stack-Entwickler und Speaker mit Begeisterung für Technologie, Architektur und Organisation.

Entwickelt und betreibt Software, datengetrieben mit Fokus auf Kundenmehrwert. Bildet sich und andere gern aus und weiter.

Stolzes Gründungsmitglied der Hacker-School.

> ”Statistik funktioniert nur dann, wenn derjenige, der die statistische Bewertung vornimmt im Mittel intelligenter ist als Zufall.” – Prof. Dr. Peter Kruse

## Mehr erfahren

### [Talks](./talks/index.md)
Ihr findet mich auf vielen deutschen Konferenzen und immer mal wieder auch International zu Themen wie DDD, Functional Programming, SPAs und vielem mehr. Kommt gern mal vorbei und sprecht mich an!

### [Experience](./experience/index.md)
Mehr als 15 Jahre Erfahrung in der Softwarearchitektur und -entwicklung. Sowohl als Entwickler, Architekt, Dozent und Führungskraft. Mein Steckenpferd ist die Vertikalisierung und das Company Rebuilding von Entwicklungsteams und Organisationen.

### [Education](./education/index.md)
Ich lese alles was mit Führung, Organisatoinsentwicklung und Softwarearchitektur sowie -entwicklung zu tun hat und bin stolzer Absolvent der Dualen Hochschule Nordakademie.

### [Skills](./skills/index.md)
Ob Führung, Backend, Frontend, Ops oder Methodiken, ich bin ein Full-Stack-All-Rounder durch und durch. Für die ganze Liste schaut gern auf die Skills page.

### [Kontakt](./kontakt/index.md)
Falls ihr mich als Speaker für eure Konferenz oder einen Podcast einladen wollt oder einfach nur Fragen habt schreibt oder ruft mich gern an!
