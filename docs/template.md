# CATEGORY

DESCRIPTION

## TOPIC

DESCRIPTION

### EXPLANATION

[:octicons-file-code-24: PRINCIPLE][1] · [:octicons-workflow-24: REFERENCE][2] · :octicons-zap-24:
SOURCE: [CodeHilite][3]

[1]: https://facelessuser.github.io/pymdown-extensions/extensions/superfences/
[2]: https://facelessuser.github.io/pymdown-extensions/extensions/superfences/
[3]: https://facelessuser.github.io/pymdown-extensions/extensions/superfences/

`KONZEPT 1`

:   DESCRIPTION[^1]

    !!! tip "TIP TO KONZEPT"
        DESCRIPTION

    ``` code
    EXAMPLE CODE
    ```

`KONZEPT 2`

:   DESCRIPTION[^2]

    === "EXAMPLE 1"

        ``` code
        EXAMPLE 1
        ```

    === "EXAMPLE 2"

        ``` code
        EXAMPLE 2
        ```

    !!! caution "IMPORTANCE OF KONZEPT"
        DESCRIPTION

### USAGE STATUS / NEXT STEPS / ADOPTION

- [x] STEP 1
- [x] STEP 2
- [ ] STEP 3
- [ ] STEP 4

[^1]:
QUELLE FÜR WEITERE INFORMATIONEN

[^2]:
QUELLE FÜR WEITERE INFORMATIONEN
