# Introduction

The `Responsible Architecture` is a modern approach to understanding [enterprise][1] and [software][2] architecture not
only as a discipline of creating the fundamental structures of a software system but as a combination
of __[product development][3]__, __[organizational methods][4]__ and __[technology guidelines][5]__ made possible
by __[transformational leadership][6]__.

[1]: https://en.wikipedia.org/wiki/Enterprise_architecture

[2]: https://en.wikipedia.org/wiki/Software_architecture

[3]: ./product/introduction.md

[4]: ./organization/introduction.md

[5]: ./technology/introduction.md

[6]: ./transformational-leadership.md

## Motivation

TODO

- KPIs (Accelerate)

- Phoenix Project
- Unicorn Project
- Lean Startup
- Accelerate
- Dev Ops Movement, Handbook

## Principles

TODO

### High Performing behaviors and practices

![](high-perf-behaviors-practices.jpg)

_This section is heavily influenced on "Accelerate" by Nicole Forsgren PhD, Jez Humble, Gene Kim.[^1]_

[^1]:
[Accelerate by Nicole Forsgren PhD, Jez Humble, Gene Kim](https://books.google.de/books/edition/b/Kax-DwAAQBAJ)
