# Transformational Leadership

The fundamental basis of the `Responsible Architecture` resides in creating
an [environment of trust](#environment-of-trust). To create an [environment of trust](#environment-of-trust) you need to
understand its [leaders characteristics](#leaders-characteristics) and follow the [behaviors](#behaviors)
of `Transformational Leadership`. Using [methods to collaborate](#methods-to-collaborate) help us get there.

## Environment of trust

In an `environment of trust` leaders inspire and motivate employees to archive higher performance by appealing to their
values and sense of purpose, facilitating wide-scale organizational change. `Transformational leaders` focus on getting
employees to identify with the organization and engage in support of organizational objectives. These leaders are best
distinguish from traditional management by following characteristics:

### Leaders Characteristics

??? summary "Vision"

    - Has a clear unterstanding of where the team is going.
    - Has a clear sense of where he/she wants the team to be in five years.
    - Has a clear idea of where the organization is going.

??? summary "Inspirational stimulation"

    - Says things that make employees proud to be part of the organization.
    - Says positive things about the team.
    - Encourages employees to see changing environments as situations full of opportunities.

??? summary "Intellectual communication"

    - Challenges employees to think about old problems in a new way.
    - Has ideas that have forced employees to rethink some things that they have never questioned before.
    - Has challenged employees to rethink some of their basic assumtions about their work.

??? summary "Supportive leadership"

    - Considers employees personal feelings before action.
    - Behaves in a manner which is thoughtul of employees personal needs.
    - Sees that the interests of employees are given due consideration.

??? summary "Personal recognition"

    - Commends employees when they do a better than everage job.
    - Acknowledges improvement in employees quality of work.
    - Personally compliments employees when they do outstanding work.

_This section is heavily influenced on "Accelerate" by Nicole Forsgren PhD, Jez Humble, Gene Kim.[^1]_

## Redwork vs. Bluework

Before we dig into the specific [behaviors](#behaviors) that create an [environment of trust](#environment-of-trust)
lets look at how the industrial age management techniques influence our mindset making it very hard to gain the
environment of trust's [leaders characteristics](#leaders-characteristics) without `Transformational Leadership`.

The industrial age company separated who did the bluework and redwork by class, into blueworkers and redworkers. We
inherited these thoughts but use different cultural signals to indicate these classes: leaders and followers, salary and
hourly, white collar and blue collar, lab coats and overalls.

> Without our knowing or thinking about it, our language and organizational structure are biased toward performing redwork.

Only by shifting between both redwork and bluework the environment of
trust's [leaders characteristics](#leaders-characteristics) can be reached. Therefore, using the [behaviors](#behaviors)
of `Transformational Leadership` are of such immense importance to creating
an [environment of trust](#environment-of-trust).

??? info "Redwork"

    Redwork is __doing__. Redwork is __clockwork__.

    Redwork consists of a constant battle for __efficiency__ and for __getting work done__ against the clock. This is why workers clock in and clock out and many people are paid "by the hour".

    People performing redwork feel the effects of this __pressure__ as stress and are __under the influence of redwork__. They cannot help it.

    Our __mindset__ in redwork is a __prove-and-perform__ mindset. The protect mindset is an unhelpful subset of the performance mindset and is to be avoided.

    __Variability__ is an __enemy__ to redwork.

??? info "Bluework"

    Bluework is __thinking__. Bluework is __cognitive work__.

    Bluework is about __creative input__ and __decision-making__. Bluework lives in service to redwork. Bluework is harder to measure based upon the time input.

    __Stress__ has a __strong negative impact__ on people trying to perform bluework.

    Our __mindset__ in bluework is an __improve and learn__ mindset.

    __Variability__ is an __ally__ to bluework.

_This section is heavily influenced on "Leadership Is Language" by L. David Marquet.[^2]_

### The redwork-bluework operating system

Implementing a `redwork-bluework operating system` means deliberately practicing the rhythm of redwork followed by
bluework.

> The rhythmic oscillation between redwork and bluework can be applied at the strategic, operational, and tactical levels. It applies to teams and individuals. It can be applied in our lives. The outcome of the redwork-bluework rhythm is learning learning at work, at home, and at life.

Leaders have three domains in which to influence the system:

#### The first domain

The first domain is determining the __overall balance__ between __red and blue__, with more frequent bluework at the
beginning of a project when there is more uncertainty and the focus should be on learning. Then they extend the length
of the redwork periods, spacing out the bluework later in the project as the focus shifts toward production and most
major decisions have been made.

#### The second domain

The second domain is __within__ the __bluework__ periods getting everyone involved in bluework rather than leadership
only, and managing the bluework periods with a goal of embracing variability.

#### The third domain

The third domain is __within__ the __redwork__ periods setting goals and a focus for the team. This is the domain
leaders will be most familiar with and which received the least attention among these behaviors. I often see ways of
doing the redwork better. There are many tools available, such as Lean, to assist leaders in this domain.

## Behaviors

To achieve the described [leaders characteristics](#leaders-characteristics) and shift between
both [redwork and bluework](#redwork-vs-bluework) leaders do not need to behave according to a traditional industrial
age management but modern `Transformational Leadership`. This new leadership's behaviors are in order:

1. [__Control the clock__](#1-control-the-clock), not obey the clock
2. [__Collaborate__](#2-collaborate), not coerce
3. [__Commit__](#3-commit), not comply
4. [__Complete__](#4-complete), not continue
5. [__Improve__](#5-improve), not prove
6. [__Connect__](#6-connect), not conform

_This section is heavily influenced on "Leadership Is Language" by L. David Marquet.[^3]_

### 1. Control the clock

???+ example "To move toward controlling the clock"

    1. Instead of preempting a pause, make a pause possible.
    2. Instead of hoping the team knows what to say, give the pause a name.
    3. Instead of pressing on with redwork, call a pause.
    4. Instead of relying on someone to singal a pause, preplan the next pause.

`Control the clock` is the __start of the cycle__. `Control the clock` is when we exit redwork and shift to bluework.

The industrial age has programmed us to obey the clock, which tends to __keep us in redwork__, feeling the __stress of
time pressure__.

Controlling the clock is about the __power of pause__; the power of our ability to control the clock rather than obeying
the clock; being __mindful and deliberate__ with our actions; and __broadening our perspectives__.

!!! warning "Teams in redwork want to continue in redwork."

Since people who are engaged in redwork often have a performance (prove or protect) mindset, it is __difficult__ for
them to
__call time-out__ on themselves. Because they want to get things done and are penalized for any delays, they do not want
to be the source of interruptions to the work. This __responsibility__ lies with the __leader__.

The team relies on the leader either to __preplan the length__ of the __redwork__ and the moment of exiting redwork or
to
__spontaneously call__ a __time-out__ during a redwork period, in essence, an audible when needed.

### 2. Collaborate

_The `Collaborate` behavior is initiated after [controlling the clock](#1-control-the-clock)._

???+ example "To move from coercion to collaboration"

    1. Vote first, then discuss.
    2. Be curious, not compelling.
    3. Invite dissent rather than drive consensus.
    4. Give information, not instructions.

??? danger "Be aware of coercion"

    Industrial age organizations assigned deciding and doing to two groups of people: blueworkers and redworkers.

    Blueworkers (management) needed to get the redworkers to follow the decisions the blueworkers decided for them.
    Blueworkers achieved this through __coercion__. Coercion seemed like an ugly word, so instead we used words like coaxed,
    goaded, prodded, influenced, motivated, and inspired. Language patterns in the coerce behavior are skewed toward the
    leader's voice.

!!! important "For collaboration, we need to let the doers be the deciders!"

    There is still bluework and redwork, but there are no blueworkers and no redworkers.

`Collaboration` requires us to __share ideas__, be __vulnerable__, and __respect__ the ideas of __others__.

`Collaboration` happens through the __questions__ we ask and requires that we admit we don't have the whole picture.
Deep down, we need to __believe others__ can contribute to our thinking and understanding of the world.

- With `collaboration`, we ask questions starting with "what" and "how".
- We invite dissent.
- We practice being curious before being compelling.

The leader's obligation is to listen to the dissenters, __not__ to stall decisions until each is convinced of the new
direction. Always stopping action because of dissent gives too much power to dissenters. It will invite blockers,
inhibit bold decision-making, and delay action.

> Language patterns in the `Collaborate` behavior are evenly spread across the team. Share of voice among team members is more equal and the Team Language Coefficient is lower.

`Collaboration` is a core process of bluework. When we resort to coercion, we get compliance. When we engage in
`collaboration`, we get [commitment](#3-commit).

!!! help "If you need help with the collaboration behavior take a look at [methods to collaborate](#methods-to-collaborate)."

### 3. Commit

_[Collaboration](#2-collaborate) sets us up for `commitment`._

???+ example "To move from compliance to commitment"

    1. Commit to learn, not (just) do.
    2. Commit actions, not beliefs.
    3. Chunk it small but do it all.

__Coercion__ results in compliance. `Commitment` is __better__ than compliance because it __releases__ discretionary
__effort__ in people. For complex, cognitive, custom teamwork, discretionary effort is everything.

!!! warning "Teams in bluework will have a tendency to stay in bluework."

    That transition point is when we run the `Commit` behavior.

At the same time, we need to inoculate ourselves against escalation of commitment, where we tend to attach ourselves to
past decisions and continue to invest in a losing course of action.

`Commitment` comes from __within__; compliance is imposed externally. `Commitment` is linked to
__intrinsic motivation__, whereas compliance is linked to extrinsic motivation.

Following `commit` we can immerse ourselves in the redwork until we [Complete](#4-complete) the next period of redwork.

### 4. Complete

_Complete marks the end of redwork and is the signal that we go back to bluework._

???+ example "To move from continuation to completion"

    1. Chunk work for freuqent completes early, few completes late.
    2. Chelebrate with, not for.
    3. Focus on behavior, not characteristics.
    4. Focus on journey, not destination.

Before we get to the [collaboration](#2-collaborate) of bluework, however, we rest and __celebrate__.
`Completion` is about a sense of progress and accomplishment. Progress feeds progress.

!!! info "The Complete behavior also lets us test our hypotheses and the decisions that we've made thus far."

When we celebrate, we want to be careful not to manipulate or to make praise be the purpose of the celebration. The
sense of __accomplishment__ should come from the __completion__ of the __task__ itself.

!!! important "Hear the whole story behind the achievement."

    When celebrating, avoid expressions like, "Great, but..." because they do not allow enough time for anyone to feel like
    their efforts were appreciated. Instead, we need to hear the story behind the achievement. This allows us insight into
    the behaviors.

    Without understanding behaviors, we are tempted to praise, not prize, and we are tempted to shortcut our observations
    toward characteristics rather than the behavior. Praising attributes like intelligence or leadership ability tends to
    program people toward risk avoidance, when we often want the opposite.

Observe the __actions__, __efforts__, and __behaviors__ that resulted in the desirable outcomes you're celebrating. This
means that when celebrating we:

- Focus on behavior, not characteristics
- Focus on journey, not destination

Executing the `Complete` behavior also gives a sense of psychological detachment from our previous actions. This sense
of "moving on" and "letting go" enables us to look dispassionately at our past actions and decisions with an eye toward
getting better.

Executing the `Complete` behavior sets us up for the [Improve](#5-improve) behavior.

### 5. Improve

???+ example "For the improve behavior, use language that invites a mental focus that is:"

    1. Forward, not backward.
    2. Outward, not inward.
    3. On the process, not on the person.
    4. On achieving excellence, not avoiding errors.

Improve is a specific behavior as well as the __objective__ of
the [redwork-bluework operating system](#the-redwork-bluework-operating-system).

`Improve` is about __reflecting__ on what we've done and __making__ it one __better__.

???+ important "Improve pits the "get better" self against the "be good" self."

    The desire of the "be good" self to defend itself will crowd out efforts to get better. In order to open ourselves for
    improvement we need to tame the fears of the "be good" self.

!!! info "Improve happens through collaboration."

The output of the `Improve` behavior is the next hypothesis to test. Improve sets us up to [commit](#3-commit) and
launches us back into redwork.

### 6. Connect

Connect is the __enabling__ behavior that makes all the other behaviors work better. The industrial age behavior is
__conform__.

???+ example "Connect is about caring. To do this:"

    1. Flatten the power gradient.
    2. Admit you don't know.
    3. Be vulnerable.
    4. Trust first.

`Connect` is about __caring__:

- caring what people __think__
- caring how people __feel__
- caring about their __personal goals__

`Connect` is not a superficial "friendship" but caring for someone else and wanting the best for them.
`Connect` is __love__.

??? important "The key concept for connect is power gradient."

    Power gradient is how we feel hierarchy in human relationships.

A __steep__ `power gradient` means my boss seems much more important than me. Salary, office size, and accessibility are
proxies for `power gradient`. We want a low and smooth power gradient. If the `power gradient` is __steep__, it makes it
hard for team members to speak truth to power.

If the `power gradient` is too __flat__, the team wastes time and energy understanding decision rights.

Part of flattening the power gradient involves leaders demonstrating __vulnerability__ and being able to admit they
__don't know__.

__Trust__ is the result of practicing transparency over time. Trust means I believe you mean well. Whether or not you do
well depends on many factors beyond just wanting to do well.

## Methods to collaborate

To excel in the [collaboration behavior](#2-collaborate) leaders can make use of the following methods:

### Share of voice

Measuring the `share of voice` by using the `Team Language Coefficient` is a great tool to verify a groups skill to
collaborate.

More evenly distributed participation and lower `Team Language Coefficients` means more peoples' voices being heard and
in general, should result in better decision making, more resilient teams, more error tolerant and adaptive
organizations.

??? summary "Calculate the Team Language Coefficient"

    The `Team Language Coefficient` is a number between 0.0 and 1.0 that represents the deviation from a perfectly even
    participation in a conversation by the team using word count or number of seconds spoken. It reduces the total
    conversation to a single number that can be databased and compared. Higher numbers occur when one or more members of a
    team are near silent or one or more members of a team monopolize the conversation. Neither of these situations bode well
    for resilient decision making so team resilience and psychological health will be represented by a lower Team Language
    Coefficient.

    Apply this formular to calculate the `Team Language Coefficient` where `n` is the number of people and `y` their share of voice:

    $$ TLC = \left(\frac{n}{n-1}\right)\left(\frac{\left(\frac{1}{n^2}\right)\sum_{i,j}\left\vert
    y_i-y_j\right\vert}{2\overline y}\right) $$

### Leaders speak last

Part of the behavior behind being curious, not compelling, is withholding your own opinion until later. The higher you
are in the organization, the more important this is because the more likely it is that people will want to align to your
position.

!!! tip "You speak last not to prove you're the leader, but because speaking last allows others to freely voice their opinions first."

### Ask better questions

1. Instead of question stacking, try __one and done__.
2. Instead of a teaching moment, try a __learning moment__.
3. Instead of a dirty question, try a __clean question__.
4. Instead of a binary question, __start__ the question __with "what"__ or __"how"__.
5. Instead of a "why" question, try __"tell me more"__.
5. Instead of self-affirming questions, try __self-educating questions__.
5. Instead of jumping to the future, start with __present__, __past__ then __future__.

??? question "What are clean questions?"

    Here's an example: Let's say a colleague has expressed frustration with another colleague and said that they are at a
    dead end when it comes to getting the other person to complete work that a project depends upon. You ask, _“Do you have
    the courage to stand up to them?”_. __That is a dirty question!__

    It's "dirty" because that question presumes your friend should confront them by speaking up, that the metaphor is "stand
    up to" instead of, say, "partner with," and finally, that the needed resource for your friend is courage. It also
    implies that it is your friend's responsibility to get this person to do their job.

    A `clean question` would eliminate those biases and would sound like this: “What do you mean by dead end?” or “What do
    you want to have happen?”. __The structure of the clean question is designed to remove your biases and preconceptions.__

    `Clean questions` are a technique specifically designed for therapy when there is a lot of time and dedicated listening
    resources. Paying attention to the biases that might be present in your questions will make your everyday questions much
    more collaborative.

??? question "What are self educating questions?"

    === "Self affirming questions"

        - Right?
        - Does that make sense?
        - All good?

    === "Self educating questions"

        - What am I missing?
        - What could go wrong?
        - What could we do better?

### Probability cards

`Probability cards` are a helpful tool to facilitate in meetings instead of asking binary questions. This is a set of
cards that display the following percentages: `1, 5, 20, 50, 80, 95, 99`. We want to focus on the outliers, the team
members with the strongest positive and negative feelings.

You all have the same seven cards. Each person picks a card and slides it to the middle of the table, facedown. Once the
votes are in, flip the cards.

Invite the outliers to speak, sharing their ideas with the group. Questions I like to ask the outliers are “What do you
see that we don't?" or "What is behind that vote?”

??? caution "When psychological safety is middle to low"

    It is sometimes better to ask the group to rationalize each outlier's position. This has the benefit of not putting
    outliers on the spot and exercises our ability to view things from another's perspective. If outliers know they will be
    put on the spot, it will reduce the tendency of people to take outlying positions.

### Dissent cards

`Dissent cards` are a tool used to change the practices of a group. They are used during the period when we are moving
from "hard to dissent” and _low psychological safety_ to "easy to dissent” and _high psychological safety_. Once dissent
is a regular part of meetings and being curious, not compelling in response to dissent is a habit as well, the cards are
no longer needed.

The `dissent cards` are `black` and `red` cards used in a ratio of five to one. Five black for every one red. Shuffle
the deck and people take a card.

If you have a red card, you have to dissent, and the card makes it safe and necessary to do so. You're not being a jerk,
the card made you do it. If you have a black card you can still dissent if you want to, but you don't have to.

!!! info "The black cards have reminders on them for how we should respond to the dissenter, by being curious, not compelling."

[^1]:
[Accelerate by Nicole Forsgren PhD, Jez Humble, Gene Kim](https://books.google.de/books/edition/b/Kax-DwAAQBAJ)

[^2]:
[Leadership Is Language by L. David Marquet](https://books.google.de/books/edition/b/2oJ6DwAAQBAJ)

[^3]:
[Leadership Is Language by L. David Marquet](https://books.google.de/books/edition/b/2oJ6DwAAQBAJ)
