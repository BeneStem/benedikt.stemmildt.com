# Resilenz und Robustheit

!!! note
    Gehen wir nach den Cloud-Schulungen an. [kann dokumentiert werden]

* Last (Anforderungen)

## Rate Limiting

Für den Frontend Proxy wird ein eigenes Rate Limit über Myra festgelegt (Request-Limit-Level). Dieses basiert
ausschließlich auf IP-Adressen und lässt eine Rate von 600 Requests pro Minute zu.

Zusätzlich ist jeder Service selbst dafür verantwortlich, ein Rate Limit für entsprechend seiner Performance zu setzen.
Da der an den Services eingehende Traffic vom Frontend Proxy kommt, muss ein generelles Rate Limit für alle Requests
konfiguriert sein.
