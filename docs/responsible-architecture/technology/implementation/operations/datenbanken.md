# Datenbanken

Es werden folgende Datenbanken verwendet:

* Postgres als SQL-Datenbank
* MongoDB als NoSQL-Datenbank

Die Entscheidung trifft die jeweilige Säule pro Service.
