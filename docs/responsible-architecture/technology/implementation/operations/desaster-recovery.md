# Desaster Recovery

Das Gesamtsystem soll nach Möglichkeit automatisch wiederhergestellt werden. + Wir unterscheiden verschiedene
Kritikalitätslevel pro Service: Bronze / Silber / Gold. Die Kritikalitätslevel werden jeweils vom entsprechenden Team
festgelegt.

!!! info
    Es ist noch zu klären, welche verschiedenen Levels es gibt, was diese bedeuten und wann diese Festlegung erfolgt.

## Weitere Gedanken

* Jeder übernimmt Verantwortung für das Runbook
* Fundierte Dokumentation für das Bereitschaftsteam -> auch ein PO sollte dazu in der Lage sein?
* Backup absolut notwendig aber wohin soll es gespeichert werden? Eventbasiert zu MBK?
* Desaster Recovery muss getestet werden
  * Automatische Tests via z.B. Chaos Monkey
  * Regelmäßige Teamübungen via Game-Day (Beispiel-Szenarien sammeln:
    * Was passiert, wenn der Account weg ist?
    * oder das Rechenzentrum?
* Post Mortems
