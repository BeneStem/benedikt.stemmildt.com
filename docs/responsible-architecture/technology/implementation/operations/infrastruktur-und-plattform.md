# Infrastruktur und Plattform

Der Shop liegt in der Google Cloud.

Die Infrastruktur wird mit Terraform gebaut.

Für das Deployment wird Cloud Run verwendet. Cloud Functions verwenden wir für kleinere Services, die keine
Business-Logik haben.

Wir gehen den Vendor-Lockin ein, weil wir so den Shop komfortabel und schnell deployen können.

Was muss noch berücksichtig werden?

* DNS
* Routing
* Chaos
