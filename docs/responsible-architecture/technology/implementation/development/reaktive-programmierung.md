# Reaktive Programmierung

Es gibt keine generelle Entscheidung für bzw. gegen reaktive Programmierung im Bezug auf den Datenbankzugriff. Die
Entscheidung liegt in der jeweiligen Anwendung im Team.

Die Erfahrung mit Spring Webflux hat gezeigt, dass die erzwungene Nebenläufigkeit von Datenbankzugriffen in der Praxis
zu Problemen führt - wenn man die Daten nicht einfach nur Anzeigen, sondern auch interpretieren will und die reaktive
Programmierweise noch sehr unbekannt ist.
