# Git

Wir verwenden als Source code management Git wobei die remote-Reposotories in GitLab gehostet werden.

## Commit-Messages

Git verlangt zwingend zu jedem Commit eine Kurze nachricht die dazu dient die einzelnen "Arbeitspakete"
zu beschreiben. Es wird darüberhinaus in unseren Standard-einstellungen mit Commitlint verlangt die Commit-Nachrichten
wie folgt aufzubauen:

```
<type>(<scope>): <subject>
<BLANK LINE>
<body>
<BLANK LINE>
<footer>
```

[Basierend auf den Konventionen von Angular.js](https://github.com/angular/angular.js/blob/master/DEVELOPERS.md#-git-commit-guidelines)

Folgende Typen sind dabei definert:

- `feat:` Ein neues Feature
- `fix:` Ein Bugfix oder Änderung an einem Feature
- `docs:` Änderungen an der Dokumentation
- `style:` Änderungen an dem Aussehen der Seite (z.B. CSS Änderungen)
- `refactor:` Eine Codeänderung die weder Bugfix noch ein Feature verändert
- `perf:` Eine Codeänderung welche die Performance anpasst
- `test:` Tests werden hinzugefügt oder angepasst
- `chore:` Änderungen am Build Prozess, CI oder Abhängigkeiten (also alle lästigen Arbeiten 🧹)

Änderungen rückgängig machen (revert):
Wenn der Commit einen vorherigen rückgängig macht, sollte er mit `revert:` anfangen, gefolgt von der `<subject>` des
ursprünglichen Commits. Im `<body>` sollte folgendes stehen: `This reverts commit <hash>.`, wo der hash von dem
entsprechenden Commit enthalten ist.

So könnte eine solche Nachricht dann aussehen:

```
feat(service): Liefert nun auch den Zeitstempel der letzen Änderung aus

Hier steht ein wundervoller optionaler Body

gefordert wie in Issue #123
```

* Der Scope ist ebenso optional wie auch die längere Beschreibung.
* Die Kurzbeschreibung muss hinter dem Doppelpunkt mit einem Großbuchstaben beginnen und ist genau 1 Zeile lang.

* Nach der Kurzbeschreibung muss eine leerzeile Folgen
* Die längere Beschreibung ist optional.<br/>
  Die Nennung des Issue ist nur dass nötig wenn der Commit nicht in einem Branch stattfindet der aus einem Issue in
  GitLab erzeugt wurde.
