# CI / CD Pipelines

Für die CI/CD Pipelines nutzen wir [GitLab](https://about.gitlab.com). Decision record: <<Gitlab CI für Build
Pipelines>>
Zusätzlich nutzen wir die von GitLab öffentlich bereitgestellten [GitLab Runner](https://docs.gitlab.com/runner/).

## Weitere Entscheidungen:

* Automatisierter Licence-Scan wird erstmal nicht verwendet im MVP

## Weitere Gedanken:

* Automatisierter publish von den OpenAPI/AsyncAPI Dokumentationen
* Verwendung Sonarcloud?
* Feature Flags wird mithilfe von Unleash in GitLab integriert
* Automatisierte Security Tests
