# Dokumentation

arc42 Dokumentation:

* Wenn du das hier liest, dann hast du die übergeordnete Dokumentation gefunden. + Glückwunsch :-) +
* Die service-spezifische Dokumentation wird aus den zugehörigen Repositories verlinkt.

Readme:

* Es werden in den Repositories der jeweiligen Services Readme-Dateien gepflegt.

Team Dokumentation:

* Die Team-Dokumentation findet ihr über die Reiter oben in der Navigationsleiste.
* Dort kann jedes Team sich seine Dokumentation so gestalten, wie jenes es für richtig hält.

Schnittstellen Dokumentation:

* Automatische Erzeugung der OpenAPI / AsyncAPI Dokumentation (z.B. via Swagger)
