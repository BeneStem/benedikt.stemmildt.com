# Programmiersprachen

Wir haben uns im Gesamtteam auf eine möglichst einheitliche Technologie- und Wahl der Programmiersprachen verständigt.
In Abstimmung mit dem Gesamtteam ist es denkbar, dass ein Team eine andere, als die hier empfohlenen Sprachen einsetzt,
solange sie ihre Entscheidung nachvollziehbar begründen können.

## Backend

Für Backend Systeme empfehlen wir ein Kotlin backend auf Basis von Quarkus.

Für unsere Backends empfehlen wir eine objekt-orientierte Sprache die zusätzlich funktionale Ideen unterstützt. Kotlin
ist aus unserer Sicht eine gute Wahl, da sie das Java bzw. JVM Ökosystem nutzt. Daher sind viele Libraries verfügbar.
Gegebenenfalls ist es denkbar auch bestehende Java-Libraries aus dem Altsystem zeitweise zu übernehmen.

Im Vergleich zu Java ist Kotlin Code prägnanter. Auch können die Domänenobjekte nach DDD in Kotlin sauber umgesetzt
werden. Im Team ist bereits Java knowhow vorhanden, daher liegt der Schritt zu Kotlin näher als zu einer voll
funktionalen Programmiersprache.

## Frontend

Wir nutzen für unser Frontend VueJs. Dies nutzen wir als für serverseitiges Rendering. Dabei greifen wir dort auf das
Backend zu, welches uns speziell für die Seite vorbereitete Daten zur Verfügung stellt.
