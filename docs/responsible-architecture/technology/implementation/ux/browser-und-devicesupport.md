# Browser und Device-Support

Wir unterteilen Browser, über die >1% Traffic reinkommt, in vier Kategorien. Die Kategorisierung richtet sich nach folgenden KPI:
- Unique User
- Anteil am gesamten Traffic
- Weitere KPIs wie Conversion Rate oder durchschnittliche Warenkörbe können bei Bedarf ergänzend herangezogen werden

Die Kategorisierung wird alle 4-6 Wochen überprüft und auf Basis der letzten 3 Monate in Relation zu den letzten 4 Wochen (als Trendindikator) bei Bedarf korrigiert. Aktueller Stand: 21. Januar 2021

| Browser | A | B | C | D |
| --- | --- | --- | --- | --- |
| Safari | 14+ | 13 |||
| Chrome | 87+ | 86 |||
| Firefox | 84+ | 83 |||
| Samsung Browser | 13+ ||||
| Edge | 87+ | 86 |||
| Internet Explorer |||| alle |

## A-Browser

Es ist sicherzustellen, dass alle Funktionen und Designs vollumfänglich korrekt dargestellt werden. Die Sicherheit ist uneingeschränkt. Jeder Bug sollte gefixt werden.

## B-Browser

Alle Funktionen sollten korrekt ausgeführt werden, das Design kann leicht abweichen. Die Sicherheit ist uneingeschränkt. Funktionelle Bugs sollten gefixt werden, rein optische Bugs nur bei besonderer Schwere.

## C-Browser

Grundsätzlich sollten Bestellungen möglich sein, alle weiteren Funktions- oder Designmangel werden akzeptiert. Die Sicherheit ist uneingeschränkt.

## D-Browser

Explizit nicht supportete Browser.

# Ursprüngliche Idee

!!! info
    Wird auf nach die Frontend Integration verschoben

Wir tendieren dazu, den IE11 nicht zu unterstützen und kein CSS Framework zu nutzen, weil mit CSS Grid & Flexbox das
meiste abgedeckt werden kann.

## Weitere Gedanken

* Mobile First
* Nutzungsstatistik im aktuellen Shop betrachten
* Browser-Testing mit Browserstack
* Umgang mit nicht unterstützten Systemen
