# Performance

!!! note
    Noch nicht besprochen aber behandelt:

* Caching
* Cropped / resized images
* Reduziere Datenbanken je modul
* Häufige Daten die sich selten verändern in memory db
* Bilder selber fast unbegrenzt cachebar
* Preise cachebar? Bei Gutscheinen nicht...
* Product-divs als ganzes cachebar
* Harte grenzen ?
* Varnish ?? GCP ??
* Gefühlt vs technisch?
* Mobile first !!
* Effiziente DB-qry..
