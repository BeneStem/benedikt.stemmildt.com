# Search Engine Optimization

🛫

## Sitemaps

In der Sitemap stehen Seiten die wir bei Suchmaschinen gelistet haben wollen. Dabei ist es wichtig zu beachten, dass die
Seiten erreichbar sind (Statuscode: 200) und keinen Canonical Link enthält. Ein Eintrag in der `sitemap.xml` besteht
mindestens aus einem `<loc>`-Eintrag. Falls man sich dazu entscheidet ein Änderungsdatum zu hinterlegen, wird der genaue
Timestamp in dem offiziell genormenten Standard genutzt. Jede Sitemap muss in der `robots.txt` eingetragen werden.
