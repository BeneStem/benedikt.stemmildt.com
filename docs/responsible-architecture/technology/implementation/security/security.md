# Security

!!! note
    Noch nicht besprochen. (Gibt aber noch eine Schulung)

* Dependency Management & Updates `new`
* OWASP Top 10 `new`
* Secrets-Management
* On & Off Boarding
* Rollen
* Configuration

## Security Skills

Wir haben gemeinsam Security Standards definiert die wir erfüllen möchten.
Diese haben wir hier dokumentiert und auch markiert welche wir als Notwendig ansehen.

!!! tip
    Es ist allerdings nicht überall definiert bis wann diese erfüllt werden sollen.


### A proper offboarding / onboarding process is in place and documented
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | In Progress |

Bisher noch keine Beschreibung hinzugefügt.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### The team has an active Security champion
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

IT security is discussed regularly in the team.
The respective champion (which is determined by the team) acts as a multiplier, takes part as possible in the regular security round meeting.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Threats are treated as "Evil user stories"
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Syntax: As an $ACTOR i perform an $ACTION to damage an $ASSET

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Least privilege principles are fulfilled
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Only the minimum necessary rights should be assigned to a subject that requests access to a resource and should be in effect for the shortest duration necessary (remember to relinquish privileges).
Granting permissions to a user beyond the scope of the necessary rights of an action can allow that user to obtain or change information in unwanted ways.
Therefore, careful delegation of access rights can limit attackers from damaging a system.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### No Credentials in Code or artifacts
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Access to the systems by unauthorized third parties is avoided.
Access to credentials must be handled by need-to-know-Principle - so a plain text credential in a widely accessible artifact MUST be avoided.
Examples like Slack webhook tokens, NewRelicKeys, etc

!!! tip
    Das Prinzip "Ein Repository ist öffentlich einsehbar" wird angewandt.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Security relevant actions are known/documented
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Attackers are interested in potential vulnerability areas of an application.
Each team should be aware of their relevant actions .

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Alle sicherheitsrelevanten Aktionen werden geloggt.
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 2 | Jedes Team | Spätestens bis MMP | In Progress |

Know what an Attacker is interested in / preparing". All security relevant actions (confidential, sensitive, data-modifying) are logged.

Umsetzung:

- Es gibt eine Dokumentation aller sicherheitsrelevanten Aktionen.
- Ein Logging und Monitoring ist dafür eingerichtet.
- Jedes Team ist für die Identifizierung solcher Aktionen selber verantwortlich.

---

### Ein automatisches Monitoring/Alerting ist für die Compliance in der Cloud eingerichtet.
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | TBD | In Progress |

An automatic monitoring and alarming system according to the CIS Benchmark compliance checks is setup and running.

Umsetzung:

- Ferenc erkundigt sich.

---

### Results of the regularly running  compliance checks are monitored regularly
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 2 | TBD | TBD | In Progress |

AWS CIS provides a list of possible best practices and recommendations for configuring AWS resources.
For each team, the team's resources are validated against CIS compliance.
Results of the regular running CIS Benchmark compliance / alarm checks will be checked contemporarily.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Automated checks for known vulnerabilities on SOFTWARE and COMPONENTS are performed
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | spätestens bis MMP | Fertig |

Prüft gegen CVE-Datenbanken, also bekannte Schwachstellen.
Regelmäßige Prüfung (täglich) bei jedem build.
Beispielhafte Auswahl an Tools:

- OWASP Dependency Checker
- NPM Audit
- Snyk
- DefectDojo
- RetireJS

!!! tip
    Herausforderung: “False Positives”

Umsetzung:

- `gradle dependencyCheck` in der GitLab CI/CD ausführen
- `npm audit` in der GitLab CI/CD ausführen

---

### Software and components are checked for known vulnerabilities
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 2 | TBD | TBD | In Progress |

Potential attack vectors are reduced by using 3rd party code.
Used libraries are searched for security issues.
Vulnerable libraries are exchanged.

Umsetzung:

- Auf die Schwachstelle wird zeitnah reagiert
- Sofern eine Schwachstelle gefunden wurde, werden pipelines blockiert

---

### Only hardened Machine Images are used
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

In order to prevent security incidents from and contain possible damage,
the attack surface of the IT system SHOULD be kept as small as possible,
whilst taking account of the specific protection requirement.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Automated Checks for (security-related) patches on SYSTEMS are performed
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | Fertig |

Automated checks for (security-related) patches and updates on SYSTEMS are performed.
Dies beinhaltet vorallem die Docker Images die wir nutzen.

Umsetzung:

- Nutzung vom Dependabot bei Docker Images
- Offizielle Docker Images nutzen
- Images werden mindestens einmal am Tag gebaut und deployt
- Selbstgebaute Docker Images werden mit einem Security Scanner überprüft (z.B. durch GCP)

---

### Systems are regularly checked for (security-related) patches and updated
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 2 | TBD | TBD | In Progress |

Systems and containers are regularly updated to u./o. safety-relevant versions tested and possibly renewed

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Basic security principles are fulfilled
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

The attack surface of data shall be kept as small as possible, taking account of the specific protection requirement.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Data is encrypted "at rest"
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | In Progress |

Es müssen alle Daten verschlüsselt werden die sich nicht in der Anwendung befinden und persistent gesichert sind.

Umsetzung:

- An allen Stellen umsetzen (jedes Team ist selbst dafür verantwortlich, bei Shared Projekten übernimmt das "Lead"-Team)
- Kubernetes:
  - Secrets müssen verschlüsselt werden

---

### Data is encrypted "in transit"
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | In Progress |

Es müssen alle Daten auf dem Transportweg verschlüsselt werden.

Umsetzung:

- Überall wird Https als Standard verwendet
- Falls kein Https möglich ist, wird auch dann verschlüsselt

---

### Relevant HTTP security headers are set
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Hardening Your HTTP Security Headers.

1. Content Security Policy (CSP)
2. X-XSS-Protection
3. HTTP Strict Transport Security (HSTS)
4. Transfer-Encoding/Content-Length 5.X-Frame-Options
6. (Expect-CT)
7. X-Content-Type-Options
8. (Feature-Policy)
9. Cache-Header (private, no-store, ...)

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Authentifizierung ist sicher umgesetzt.
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | In Progress |

Authentifizierung interne Systeme:

- Git-Repo,
- …

Authentifizierung Kunden(shop)

TBD. Backup: Passwords SHOULD satisfy the  password quality guidelines (complexity, period of validity, re-use).

[Weitere Informationen](https://cheatsheetseries.owasp.org/cheatsheets/Authentication_Cheat_Sheet.html)

Umsetzung:

- intern:
  - mfa überall aktiviert
  - nicht eratbare passwörter (aus passwortmanager)
  - passwörter nicht mehrfach verwenden
- shop:
  - empfehlung → oauth2 + jwt
- machine to machine:
  - oauth2 + jwt

---

### The team is aware about (and tracks) confidential content(s)
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Content marked as "Confidential" is of interest for an attacker - normally data which results of a "Protection needs determination" should be tracked/marked.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Confidential data (“Assets”) is deleted when no longer needed
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | TBD | In Progress |

- Liste relevanter Daten insbesondere Kundendaten und wo diese liegen
- siehe Offboarding
- Accountpflege (Kunden)
- Logs (24h/7Tage bei ip, persönlkichen Daten)

Umsetzung:

- offboarding prozess mus dokumentiert sein (je team)
- DSGVO anforderungen sind umgesetzt
- Alle log daten müssen innerhalb von 7 Tagen gelöscht werden (tbd mit bene besprechen)

---

### Do not expose sensitive (confidential/"Assets") data
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | Jedes Team | Spätestens bis MMP | In Progress |

OWASP: Applications can unintentionally leak information about their configuration, internal workings, or violate privacy through a variety of application problems.
Applications can also leak internal state via how long they take to process certain operations or via different responses to differing inputs, such as displaying the same error text with different error numbers.
Web applications will often leak information about their internal state through detailed or debug error messages.
Often, this information can be leveraged to launch or even automate more powerful attacks.

Umsetzung:

- Interne Endpunkte sind nicht öffentlich erreichbar
- Keine sensiblen Informationen sind in ausgespielten Fehlermeldungen enthalten (z.B. Stacktrace, mögliche/invalide Parameter)

---

### All accesses to (confidential/sensitive) services are personalized
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Keine Gruppenaccounts!
Jeder Zugriff kann personalisiert zugeordnet werden.
Kommt auch aus DSGVO/”TOM”s
Nur Zugriff auf Kundendaten wenn notwendig. Auch “Least Privilege”

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Provide baseline intrusion prevention mechanisms
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

According to identified trust boundaries (see "Threat Analysis") each system/service should perform a baseline protection.
This includes input validation against malformed or malicious (user-generated) content, rate limiting, ...

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Regular external penetration tests are performed
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Conducted by external service provider.
Blackbox/Graybox/Whitebox.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Regular manual internal penetration tests are performed ("Quick Checks")
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

Performed internally - to get an idea of the attack surface seen from an attackers' perspective.
=> OWASP Testing Guide (v4)

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Automated penetration test(s) are performed
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

- ZAP/Burp Scanner sind vorhanden
- Manueller Test wird durchgeführt
- Skript daraus zur Automatisierung

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Team members participate in team or topic specific hacking
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

This skill acts as an addon to the threat analysis.
Each team has its own attack surface (e.g. determined by a threat modeling) - and this attack surface should be technically analyzed for existing / potential vulnerabilities.
A pentest with a small, predefined subset of attacks.
Might result in creation of automatic tests.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### Team participates in Security training(s)
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Nein | 1 | TBD | TBD | In Progress |

At least one colleague per team participates on web application related trainings.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### The secure architecture principles are fulfilled
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |


Generally, the key objectives within a secure architecture includes:

- Confidentiality: Ensuring that information is not accessed by unauthorized persons (e.g., access control or encryption)
- Integrity: Ensuring that information is not altered by unauthorized persons in a way that is not detectable by authorized users (e.g., access control or checksums)
- Availability: Ensuring timely and reliable access to and use of information and preventing unauthorized withholding of information.
- Authenticity: Ensuring that users are the persons they claim to be.
- Authorization: What information is an authenticated user allowed to access or which operations allowed to perform

To ensure these objectives, the following secure architecture principles should be followed:

- Access Control
- Isolation
- Principle of Least Privilege
- Defense in depth
- Use more than one security mechanism – Secure the weakest link
- Fail securely
- Keep it simple

Umsetzung:

Zurzeit ist keine Umsetzung definiert.

---

### The team conducts "threat analysis workshops" regularly
| Notwendig | Reifegrad | Verantwortlichkeit | Deadline | Status |
| --- | --- | --- | --- | --- |
| Ja | 1 | TBD | TBD | In Progress |

Threat analyzes identify potential attack surfaces, their criticality and possible measures.
Each team defines the extent to which they are scheduled for each development cycle: per quarter (and then perimeter), per feature, or per story.

Umsetzung:

Zurzeit ist keine Umsetzung definiert.
