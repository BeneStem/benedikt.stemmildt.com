# URL-Structure, Naming und Name-Spacing

## CSS-Klassen

Zur Benennung der CSS-Klassen folgen wir einem eindeutigen Schema. Mit diesem Schema soll unter anderem verhindert
werden, das Teams unabhängig voneinander Styles verwenden können. Dies wird durch einen Team Prefix in dem Schema
realisiert. Die konkrete Namenskonvention findet ihr [hier](namenskonvention.md).

## Internationalisierung

Grundsätzliche URL-Struktur: `https://<Subdomain>.TODO.de`

Wenn kein Ländercode hinter der Top-Level-Domain gesetzt ist, ist die URL Deutschland zugeordnet.

Die internationalisierten URLs nutzen '.com' als TLD oder beginnen nach '.de' mit einem Ländercode
(z. B. 'at') oder einem Locale (z. B. 'ch-de').

ACH-Variante: `https://<Subdomain>.TODO.de/at, https://<Subdomain>.TODO.de/ch-de`

Internationale Variante: `https://<Subdomain>.TODO.com/uk, https://<Subdomain>.TODO.com/fr`

## URL-Schema

- Interne URLs für Services: `<environment>.<service>.<team>.ecom.TODO.de`

- Die APIs der Services sind unter `www.TODO.de/api/<team>/<service>/...` erreichbar

- Protokoll muss immer https sein!

- Die URLs sind immer ohne Trailing-Slash.

- Ausgeschriebene Wörter innerhalb der Verzeichnisstruktur auf deutsch.

- Bezeichner der ersten Verzeichnisebene (ggf. nach Ländercode oder Locale) werden einem bestimmten Team zugeordnet. Der
  Verzeichnisname muss "shopweit" eindeutig sein.

- Indexierte Sites sollten eine Schachtelungstiefe von drei Verzeichnissen nicht überschreiten
  (Empfehlung von Claneo)

## Parameter

Parameter haben sprechende Namen und sind auf deutsch (z.B. ?durchmesser=30cm&farbe=rot)

## Mehrsprachigkeit

Andere Sprachen werden über URL-Parameter gelöst (z.B. '?lang=en') und im Local Storage gespeichert.

Im HTML-Code ist immer das lang-Attribut gesetzt (`<html lang="de">`)

!!! info
    Vertagt, da mehr über die Architektur und Fachlichkeit bekannt sein muss.
