# Integration

## Backend

!!! info
    Besprechen wir am 26.10.

## Frontend

Design System

Jedes Team hat ebenfalls ein eigenes Storybook, indem nur eigene Komponenten gepflegt werden. Sobald eine Komponente von
einem anderen Team genutzt werden soll, muss es in das übergreifende Design-System eingepflegt werden.

## DWH

* Aktuelles DWH muss weiterhin befüllt werden.
  * legacy: direkte DB-Schnittstelle
* CRM-DWH wird analog backend integriert
