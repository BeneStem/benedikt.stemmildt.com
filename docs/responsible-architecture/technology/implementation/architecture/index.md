# Concepts
# Konzepte

Quellen & Hilfestellungen:

* 12 factor app
* Reactive Programming / Reactive Manifesto
* Testpyramide
* Arc 42 / ASCII doc / Swagger
* micro frontends
* self contained systems
* https://isa-principles.org/
* DDD / Ports & Adapters / Hexagonale Architektur
* Json Webtoken / OAuth
