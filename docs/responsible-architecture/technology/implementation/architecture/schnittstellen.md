# Schnittstellen

Wir nutzen Google Cloud Functions als Adapter, um externe Schnittstellen (z.B. ERP, PIM, etc.) mit unserem Service zu
verbinden, wenn es Sinn ergibt.

Für interne Schnittstellen nutzen wir REST, da es hierfür ein umfangreiches Tooling (Swagger-UI) gibt und genug
Erfahrungen vorhanden sind.

gRPC nutzen wir vorerst nicht, da die Integration in Quarkus noch nicht rund ist und noch eine geringe Popularität hat.

Es kann im Team mit übergreifender Absprache entschieden werden, ob eine andere Implementierung (GraphQL, gRPC, ...)
verwendet werden kann, wenn es für den Service effektiver ist.

!!! note
    Noch nicht besprochen/entschieden ist die Nutzung von Kafka als Replikationsmechanismus.
