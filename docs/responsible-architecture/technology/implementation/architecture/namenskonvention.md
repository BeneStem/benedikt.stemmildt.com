# Namenskonvention

## Terraform

Resourcen sollten eindeutig benannt werden, dies beinhaltet zum Beispiel den Teamnamen, Säulennamen und/oder
Servicenamen. Wir möchten uns an gängige Namenskonventionen halten.

Namen schreiben wir in snake_case.

```terraform
variable "region_test" {
  type = string
  default = "europe-west3"
}
```

## CSS

Für CSS nutzen wir eine eindeutige Namenskonvention basierend auf [BEM Standard](http://getbem.com/introduction/).
Dadurch bildet sich ein Namensschema wie folgt:

```
<Team_Prefix>_<Block>__<Element (optional)>--<Styling (optional)>
Beispiele:
sha_button                      (Ein Block)
sha_button--background          (Ein Block mit Styling)
sha_button__center              (Ein Block mit einem verschachteltem Element)
sha_button__center--background  (Eine vollwertige Klasse nach Namensschema)

//Mehrere Wörter sind mit einem Bindestrich zu trennen.
```

!!! tip
    Dabei ist es wichtig, seinen Teamprefix mit anzugeben,
    da auch bei scoped styles es vorkommen könnte,
    das Styling durch andere überschrieben werden könnte.
