# Identitätsmanagement

Wir wollen die GoogleCloud Implementierung ausprobieren (gAuth?). Diese basiert auf OAuth, aber nimmt uns den komplexen
Einrichtungsaufwand ab. Das entscheidet später final das umsetzende Team 3.

Session Tokens schließen wir aufgrund der synchronen Validierung aus.

## Session Token

| Pro | Contra |
| --- | --- |
| einfach zu implementieren | synchrone Validierung |
| kann zurückgezogen werden | |

## JWT

| Pro | Contra |
| --- | --- |
| asynchrone Validierung | kann nicht zurückgezogen werden |
| Rollen können encodiert werden | |

## OAuth

| Pro | Contra |
| --- | --- |
| komplexe Berechtigungen können abgebildet werden | komplexe Implementierung |
| Single Sign On über verschiedene Anbieter | kann nicht zurückgezogen werden |
| Social Logins möglich | |
| Integration mit 3rd Party Services | |
